﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class TuringMachine : MonoBehaviour {

	public string cell;

	public List<string> tape;
	public string state;
	public string endState;

	private JsonData instructions;

	private string jsonString;
	private JsonData itemData;

	// Use this for initialization
	void Start () {

		jsonString = File.ReadAllText (Application.dataPath + "/Instructions.json");
		instructions = JsonMapper.ToObject (jsonString);
//		Debug.Log(instructions["q0"]["1"]["write"]);
//		Debug.Log(instructions[state]["1"]["write"]);

//		for (int i = 0; i < 5; i++) {
//			GameObject cellClone = Instantiate(cell, new Vector3(i,0,0), Quaternion.identity) as GameObject;
//		}

		Run ();
	
	}

	void Run() {
		int index = 0;
		while(state != endState) {
			if(index < tape.Count) {
				cell = tape[index];
			} else {
				cell = "B";
				tape.Add("B");
			}
			tape [index] = instructions [state] [cell] ["write"].ToString();
			index += int.Parse (instructions [state] [cell] ["move"].ToString());
			state = instructions[state][cell]["nextState"].ToString();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
